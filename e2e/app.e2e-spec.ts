import { ANGULAR2EXAMPLESPage } from './app.po';

describe('angular2-examples App', function() {
  let page: ANGULAR2EXAMPLESPage;

  beforeEach(() => {
    page = new ANGULAR2EXAMPLESPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
