import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { ExternalAPIService } from '../../services/external-api.service';

import { SchoolInfoObject } from '../../domain/SchoolInfoObject';

@Component({
  selector: 'app-button',
  templateUrl: './button-view.component.html',
  styles: []
})
export class ButtonViewComponent implements OnInit {
  schools: SchoolInfoObject;
  schoolInfo: SchoolInfoObject;

  constructor(private externalAPIService:ExternalAPIService, private location: Location) { }

  ngOnInit() {
  }

  onClick(){
    console.log("The button has been clicked");
  }

  /**
   * Event to obtain a JSON Array containing the list of schools available
   */
  onClickExtAPIJsonArray(){
    this.externalAPIService.getSchools().subscribe(
      response => {this.schools = response.list; console.log(this.schools)},
      error => console.error('Error' + error),
      () => console.log('Completed',this.schools)
    )  
  }

  /**
   * Event to obtain a JSON Object containing the information of a school
   */
  onClickExtAPIJsonObject() {
    this.externalAPIService.getSchoolInfo().subscribe(
      response => this.schoolInfo = response,
      error => console.error('Error' + error),
      () => console.log('Completed')
    )
  }

  /**
   * Returns to the previous page
   */
  goBack(){
    this.location.back();
  }

}
