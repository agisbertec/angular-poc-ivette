import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { SchoolInfoObject } from '../../domain/SchoolInfoObject';

import { ExternalAPIService } from '../../services/external-api.service';

@Component({
    selector: 'app-table-view',
    templateUrl: './table-view.component.html'
})
export class TableViewComponent implements OnInit {
    schools: SchoolInfoObject[];
    schoolColumns: any[];

    constructor(private externalAPIService:ExternalAPIService, private location:Location) { }

    /**
     * This function is executed every time the component is loaded.
     * It loads all of the needed data.
     */
    ngOnInit() {
        this.loadSchools();
        this.loadColumns();
    }

    /**
     * Loads the list of schools available
     */
    loadSchools() {
        this.externalAPIService.getSchools().subscribe(
        response => {this.schools = response.list; console.log(this.schools)},
        error => console.error('Error' + error),
        () => console.log('Completed',this.schools)
        )  
    }

    /**
     * Loads the information of the columns that will be displayed for each school available
     */
    loadColumns(){
        this.schoolColumns = [
            {field: 'name', header: 'Name', sortable: true, filter: true, filterMatchMode: 'endsWith'},
            {field: 'description', header: 'Description', sortable: true, filter: true, filterMatchMode: 'contains'},
            {field: 'address', header: 'Address', sortable: true, filter: false, filterMatchMode: ''}
        ];
    }

    /**
     * Returns to the previous page
     */
    goBack(){
        this.location.back();
    }
}