import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { MainViewComponent } from './views/main-view/main-view.component';
import { ButtonViewComponent } from './views/button-view/button-view.component';
import { TableViewComponent } from './views/table-view/table-view.component';

const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full' },
  { path: 'main',  component: MainViewComponent },
  { path: 'main/buttons',  component: ButtonViewComponent },
  { path: 'main/table',  component: TableViewComponent }
 // { path: 'schools/schoolInfo/:name',     component: SchoolInfoComponent }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {

}
