import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';

import {DataTableModule,SharedModule} from 'primeng/primeng';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: []
})
export class TableComponent implements OnInit {
  @Input() itemList: any[];
  @Input() itemColumns;
  @Input() numberRows;
  @Input() isPagination;
  @Input() isGlobalFilter;

  constructor(private location:Location) { }

  ngOnInit() {

  }

}
