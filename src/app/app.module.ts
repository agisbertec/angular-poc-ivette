import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import {DataTableModule} from 'primeng/primeng';

import { AppComponent } from './app.component';
import { MainViewComponent } from './views/main-view/main-view.component';
import { ButtonViewComponent } from './views/button-view/button-view.component';
import { TableComponent } from './components/table/table.component';
import { TableViewComponent } from './views/table-view/table-view.component';

import { ExternalAPIService } from './services/external-api.service';


@NgModule({
  declarations: [
    AppComponent,
    MainViewComponent,
    ButtonViewComponent,
    TableComponent,
    TableViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule, 
    DataTableModule
  ],
  providers: [ExternalAPIService],
  bootstrap: [AppComponent]
})
export class AppModule { }
