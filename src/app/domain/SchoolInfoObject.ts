export interface SchoolInfoObject {
  name;
  description;
  address;
}