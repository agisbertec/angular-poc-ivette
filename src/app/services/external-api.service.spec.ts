/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ExternalAPIService } from './external-api.service';

describe('ExternalAPIService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExternalAPIService]
    });
  });

  it('should ...', inject([ExternalAPIService], (service: ExternalAPIService) => {
    expect(service).toBeTruthy();
  }));
});
