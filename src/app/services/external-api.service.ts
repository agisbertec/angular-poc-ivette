import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable }     from 'rxjs/Observable';

import { SchoolInfoObject } from '../domain/schoolInfoObject';

@Injectable()
export class ExternalAPIService {

  constructor(private http: Http) { }

  /**
  * Calls a Rest Service that returns a JSON Array that contains the information of all of the Schools available 
  */
  getSchools(){
    // The result has the following structure: {"list":[{"name":"xxx", "description"="xxx", "address"="xxx"},{"name":"xxx", "description"="xxx", "address"="xxx"}]}
    return this.http.get("http://localhost:8081/schools").map(res => res.json());
  }

  /**
  * Calls a Rest Service that returns a JSON Array that contains the information of all of the Schools available 
  */
  getSchoolsNoItems(){
    // The result is an empty list.
    return this.http.get("http://localhost:8081/schoolsNoItems").map(res => res.json());
  }

  /**
   * Calls a Rest Service that returns a JSON Object that contains the information of a single School
   */
  getSchoolInfo(){
    // The result has the following structure: {"name":"xxx", "description":"xxx", "address":"xxx"}
    return this.http.get("http://localhost:8081/schoolInfo").map(res => this.toSchool(res.json()));
  }

  /**
   * Used to map the resulting JSON into a SchoolInfoObject
   */
  toSchool(responseObject): SchoolInfoObject{
    let schoolInfo = <SchoolInfoObject>({
      name: responseObject.name,
      description: responseObject.description,
      address: responseObject.address
    });

    return schoolInfo;
  }

  

}
